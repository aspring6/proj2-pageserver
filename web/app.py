from flask import Flask, render_template, request, abort


app = Flask(__name__)


def checkForInvalid():
    '''Should only be called within @app.route, otherwise, url will be null'''
    data = request.url
    begin = 0
    #http is at beginning of request (as it should be)
    if (data.find("http://") == 0):
        #Begin looking after http:// (start @ index 7)
        begin = 7
    invalid_slashes = data.find("//", begin)
    invalid_parent = data.find("..", begin)
    invalid_root = data.find("~", begin)
    if (invalid_parent + invalid_root + invalid_slashes == -3):
        return False
    else:
        return True
        

@app.route("/")
def hello():
    if checkForInvalid():
        return render_template('403.html'), 403
    return "UOCIS docker demo!!"


@app.route("/trivia.html")
def trivia():
    if checkForInvalid():
        return render_template('403.html'), 403
    return render_template('trivia.html')


@app.errorhandler(404)
def page_not_found(e):
    if checkForInvalid():
        return render_template('403.html'), 403
    return render_template('404.html'), 404


@app.errorhandler(403)
def forbidden(e):
    return render_template('403.html'), 403



if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
