# Proj2-Page Server
This project handles GET requests to a server, sending the according response back depending on if the page is present or not. This project exercises the use of Docker and Flask.

## Author: Alec Springel, aspring6@uoregon.edu ##

# Usage
First, build the flask image with:  
```docker build -t uocis-flask-demo .```  
To run the program run:  
```docker run -d -p 5000:5000 uocis-flask-demo``` and connect via a web browser